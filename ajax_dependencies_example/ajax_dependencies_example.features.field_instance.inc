<?php
/**
 * @file
 * ajax_dependencies_example.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ajax_dependencies_example_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-ajax_dependencies_example-field_ajax_dependencies_ex_spec'
  $field_instances['node-ajax_dependencies_example-field_ajax_dependencies_ex_spec'] = array(
    'bundle' => 'ajax_dependencies_example',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ajax_dependencies_ex_spec',
    'label' => 'Specify',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => -3,
    ),
  );

  // Exported field_instance:
  // 'node-ajax_dependencies_example-field_ajax_dependencies_ex_type'
  $field_instances['node-ajax_dependencies_example-field_ajax_dependencies_ex_type'] = array(
    'bundle' => 'ajax_dependencies_example',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ajax_dependencies_ex_type',
    'label' => 'Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Specify');
  t('Type');

  return $field_instances;
}
