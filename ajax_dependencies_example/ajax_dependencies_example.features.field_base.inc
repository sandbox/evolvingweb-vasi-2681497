<?php
/**
 * @file
 * ajax_dependencies_example.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ajax_dependencies_example_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_ajax_dependencies_ex_spec'
  $field_bases['field_ajax_dependencies_ex_spec'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ajax_dependencies_ex_spec',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 64,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_ajax_dependencies_ex_type'
  $field_bases['field_ajax_dependencies_ex_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ajax_dependencies_ex_type',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'fruit' => 'Fruit',
        'vegetable' => 'Vegetable',
        'other' => 'Other',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
