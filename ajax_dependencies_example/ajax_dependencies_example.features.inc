<?php
/**
 * @file
 * ajax_dependencies_example.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ajax_dependencies_example_node_info() {
  $items = array(
    'ajax_dependencies_example' => array(
      'name' => t('AJAX Dependencies Example'),
      'base' => 'node_content',
      'description' => t('A sample type for AJAX dependencies.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
