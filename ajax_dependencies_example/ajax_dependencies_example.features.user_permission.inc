<?php
/**
 * @file
 * ajax_dependencies_example.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ajax_dependencies_example_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ajax_dependencies_example content'.
  $permissions['create ajax_dependencies_example content'] = array(
    'name' => 'create ajax_dependencies_example content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ajax_dependencies_example content'.
  $permissions['edit any ajax_dependencies_example content'] = array(
    'name' => 'edit any ajax_dependencies_example content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
