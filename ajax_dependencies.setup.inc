<?php

/**
 * @file
 * File for class AjaxDependenciesSetup.
 */

/**
 * A class to setup the AJAX dependencies on a form.
 */
class AjaxDependenciesSetup {
  /**
   * Setup AJAX dependencies on a form.
   *
   * @param array $form
   *   The complete form to setup.
   */
  public static function setupForm(array &$form) {
    if (isset($form['#ajax_dependencies_processed'])) {
      return;
    }

    $setup = new self($form);
    $setup->processDependencies($form, array());
    $form['#ajax_dependencies_processed'] = TRUE;
  }

  /**
   * The complete form to operate on.
   *
   * @var array
   */
  protected $form;

  /**
   * A set of existing wrapper IDs.
   *
   * @var array
   */
  protected $wrapperIds = array();

  /**
   * A cache of dependencies.
   *
   * The keys are a serialization of a parents array.
   *
   * @var array
   */
  protected $dependencies = array();

  /**
   * Constructor.
   */
  protected function __construct(&$form) {
    $this->form =& $form;
  }

  /**
   * Find AJAX dependencies on a single element and its children.
   */
  protected function processDependencies(&$element, $parents) {
    if (isset($element['#ajax_dependencies'])) {
      $this->setupElement($element, $parents);
    }

    // Recurse into the children.
    foreach (element_children($element) as $key) {
      $child_parents = $parents;
      $child_parents[] = $key;
      $this->processDependencies($element[$key], $child_parents);
    }
  }

  /**
   * Get a unique ID for an element's wrapper.
   */
  protected function wrapperId($parents) {
    $base = implode('-', $parents);

    // Replace bad characters.
    $base = preg_replace('/[^-.\w]+/', '-', $base);
    $base = 'wrapper-' . $base;

    // Ensure it's unique.
    $id = $base;
    $ctr = 2;
    while (isset($this->wrapperIds[$id])) {
      $id = "$base-$ctr";
      $ctr++;
    }

    $this->wrapperIds[$id] = TRUE;
    return $id;
  }

  /**
   * Setup AJAX dependencies for a single element.
   */
  protected function setupElement(&$element, $parents) {
    // Setup a wrapper around this element, so we know what to replace.
    $id = $this->wrapperId($parents);
    if (!isset($element['#prefix'])) {
      $element['#prefix'] = '';
    }
    $element['#prefix'] = "<div id=\"$id\">" . $element['#prefix'];
    if (!isset($element['#suffix'])) {
      $element['#suffix'] = '';
    }
    $element['#suffix'] .= '</div>';

    // Tell each dependency about the reverse dependency.
    $deps = $this->findDependencies($parents);
    foreach ($deps as $dep) {
      $dep_element =& drupal_array_get_nested_value($this->form, $dep);
      $dep_element['#ajax_dependencies_reverse'][] = array(
        'id' => $id,
        'parents' => $parents,
      );

      // Preserve any previous callback.
      if (isset($dep_element['#ajax']['callback'])) {
        $old_callback = $dep_element['#ajax']['callback'];
        if ($old_callback != 'ajax_dependencies_ajax_callback') {
          $dep_element['#ajax_dependencies_old_callback'] = $old_callback;
        }
      }

      $dep_element['#ajax']['callback'] = 'ajax_dependencies_ajax_callback';
    }
  }

  /**
   * Find the dependencies for an element.
   */
  protected function findDependencies($parents) {
    // Check the cache first.
    $cache_key = serialize($parents);
    if (isset($this->dependencies[$cache_key])) {
      return $this->dependencies[$cache_key];
    }

    $deps = array();
    $element = drupal_array_get_nested_value($this->form, $parents);
    if (isset($element['#ajax_dependencies'])) {
      foreach ($element['#ajax_dependencies'] as $spec) {
        // Expand the spec into a parents array.
        $dep_parents = $this->resolveSpec($spec, $parents);
        if ($dep_parents) {
          $deps[serialize($dep_parents)] = $dep_parents;

          // Don't recurse into ourselves!
          if ($dep_parents != $parents) {
            $deps = array_merge($deps, $this->findDependencies($dep_parents));
          }
        }
      }
    }

    $this->dependencies[$cache_key] = $deps;
    return $deps;
  }

  /**
   * Resolve a dependency specification into a parents array.
   */
  protected function resolveSpec($spec, $parents) {
    if ($spec == AJAX_DEPENDENCIES_SELF) {
      return $parents;
    }

    // Expand a string spec into an array.
    if (is_string($spec)) {
      $spec = explode('/', $spec);
    }

    // Allow modules to change it.
    $key = key($spec);
    if ($key !== 0 && $key !== NULL) {
      $context = array('form' => $this->form, 'parents' => $parents);
      drupal_alter('ajax_dependencies_spec', $spec, $key, $context);
    }

    // Check if it ended up reasonable.
    $key = key($spec);
    if ($key === 0 || $key === NULL) {
      return $spec;
    }
    return NULL;
  }

}
