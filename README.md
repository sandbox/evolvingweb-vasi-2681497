# AJAX Dependencies

## Overview

Drupal's AJAX forms support is pretty nifty! But it's harder than it has to be, requiring manual element wrapping and custom callbacks even for the simplest form.

The `ajax_dependencies` module makes it easy. Just declare which form elements depend on each other, and it takes care of the rest. Every time an element changes, every element dependent on it will be replaced via AJAX.

This module deals easily with even complicated cases. If multiple elements need to change at once, that just works. If dependencies chain—A depends on B depends on C—the implicit dependency of A on C will just work. 

## Example

Here's a small example. When an option is chosen from the select list `foo`, the element `bar` will be filled in with a message.

```php
function ajax_dependencies_example_readme($form, $form_state) {
  // Create a select list.
  $form['foo'] = array(
    '#type' => 'select',
    '#empty_value' => '',
    '#options' => array('a' => t('a'), 'b' => t('b')),
  );

  // Generate a message.
  if (!empty($form_state['values']['foo'])) {
    $message = 'You chose ' . $form_state['values']['foo'];
  }
  $form['bar'] = array(
    '#markup' => isset($message) ? $message : '',

    // Declare that this element depends on 'foo'.
    '#ajax_dependencies' => array('foo'),
  );

  // Setup the AJAX handling.
  ajax_dependencies_setup($form);
  return $form;
}
```

For many more examples, see the sub-module `ajax_dependencies_example`. You can find more info about the `#ajax_dependencies` property in the API documentation for `ajax_dependencies_setup()`.

## Caveats

* This module only works with forms. Other sorts of AJAX are out of its scope.
* This module doesn't degrade gracefully in the absence of JavaScript. It should be possible to make that happen, so contributions are welcome.
