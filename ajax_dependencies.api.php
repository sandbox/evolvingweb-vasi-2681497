<?php

/**
 * @file
 * Hooks defined by ajax_dependencies.
 */

/**
 * Alter an AJAX dependency specification.
 *
 * Dependencies specified in #ajax_dependencies are usually an array of
 * element parents. But specs can also be keyed arrays with arbitrary
 * contents.
 *
 * Implementors of this hook should attempt to turn $spec into an array of
 * element parents.
 *
 * @param array $spec
 *   An arbitrary specification of a form element.
 * @param string $type
 *   An indicator of the type of specification this is.
 *   This is also the value of the first key of $spec.
 * @param array $context
 *   Extra information, including:
 *   - "form": The complete form being processed.
 *   - "parents": The parents array of the element being processed.
 */
function hook_ajax_dependencies_spec_alter(array &$spec, $type, array $context) {
  if ($type == 'my_type') {
    $spec = array('some', 'path', 'including', $spec[$type]);
  }
}

/**
 * Alter an AJAX response when using ajax_dependencies.
 *
 * You can use this to add commands to the AJAX response.
 *
 * @param array $ajax
 *   An AJAX response array, see ajax_render().
 * @param array $form
 *   The form for this AJAX request.
 * @param array $form_state
 *   The form state of this AJAX request.
 */
function hook_ajax_dependencies_response_alter(array &$ajax, array $form,
  array $form_state) {
  $ajax['#commands'][] = ajax_command_alert("This alert will be shown!");
}
